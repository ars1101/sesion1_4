import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:session1_4/auth/data/repository/supawidgets.dart';
import 'package:session1_4/auth/prestation/pages/forgotpass.dart';
import 'package:session1_4/auth/prestation/pages/otp.dart';
import 'package:session1_4/auth/prestation/pages/signin.dart';
import 'package:session1_4/auth/prestation/pages/signup.dart';
import 'package:session1_4/auth/prestation/widgets/textfield.dart';
import 'package:session1_4/core/color.dart';
import 'package:session1_4/home/presentation/pages/profile.dart';
import 'package:session1_4/onboarding/presentation/holder.dart';
import 'package:session1_4/onboarding/repository/queue.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

User? user;

class _HomeState extends State<Home> {
  @override
  void initState() {}

  int ind = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(currentIndex: ind,
          items: [BottomNavigationBarItem(
            icon: Image.asset(
                (ind == 0) ? "assets/house-2-1.png" : "assets/house-2.png"),
            label: "Home",
          ),
            BottomNavigationBarItem(
                icon: Image.asset((ind == 1)?'assets/wallet-3.png': 'assets/wallet-3.png' ), label: "Wallet"),
            BottomNavigationBarItem(
                icon: Image.asset((ind == 2)
                    ? "assets/smart-car-1.png"
                    : "assets/smart-car.png"),
                label: "Tracking"),
            BottomNavigationBarItem(
                icon: Image.asset((ind == 3)
                    ? "assets/profile-circle-1.png"
                    : "assets/profile-circle.png"),
                label: "profile")],
          selectedItemColor: (Color(0xFF0560FA)),
          unselectedItemColor: Color(0xFFA7A7A7),
          unselectedLabelStyle: TextStyle(color: Color(0xFFA7A7A7)),
          selectedLabelStyle: TextStyle(color: Color(0xFF0560FA)),
          showUnselectedLabels: true,
          showSelectedLabels: true,
          onTap: (inda) {
            setState(() {
              ind = inda;
            });
          }),
      body: [Center(child: FilledButton(child: Text('To Order'), onPressed: (){Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => Holder()));},),), SizedBox(), SizedBox(), Profile()][ind],
    );
  }
}
