import 'dart:math';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:session1_4/auth/data/repository/supawidgets.dart';
import 'package:session1_4/auth/prestation/pages/forgotpass.dart';
import 'package:session1_4/auth/prestation/pages/otp.dart';
import 'package:session1_4/auth/prestation/pages/signin.dart';
import 'package:session1_4/auth/prestation/pages/signup.dart';
import 'package:session1_4/auth/prestation/widgets/textfield.dart';
import 'package:session1_4/core/color.dart';
import 'package:session1_4/onboarding/presentation/holder.dart';
import 'package:session1_4/onboarding/repository/queue.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:pinput/pinput.dart';

class NewPass extends StatefulWidget {
  const NewPass({super.key});

  @override
  State<NewPass> createState() => _NewPassState();
}

User? user;

class _NewPassState extends State<NewPass> {
  @override
  void initState() {}
  Uri pdf = Uri.parse(
      'https://lwtgrvmdvlhofehpmgtb.supabase.co/storage/v1/object/public/policy/rud%20(1).pdf?t=2024-02-13T19%3A55%3A19.306Z');
  TextEditingController pass = TextEditingController();
  TextEditingController passc = TextEditingController();
  bool obs = true;
  bool check = false;

  bool isValid() {
    if ((pass.text.length == 6) & (pass.text == passc.text)) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 24,
            ),
            Column(
              children: [
                SizedBox(
                  height: 78,
                ),
                Text(
                  'New Password',
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                SizedBox(
                  height: 8,
                ),
                Text(
                  'Enter New Password',
                  style: Theme.of(context).textTheme.titleSmall,
                ),
                SizedBox(
                  height: 52,
                ),
                SizedBox(
                    height: 72,
                    width: MediaQuery.of(context).size.width - 48,
                    child: CustomTextField(
                        onChange: (te) {
                          setState(() {});
                        },
                        label: 'Password',
                        hint: '**********',
                        isObscure: obs,
                        controller: pass,
                        onTapSuffix: () {
                          setState(() {
                            if (obs == true) {
                              obs = false;
                            } else {
                              obs = true;
                            }
                          });
                        })),
                SizedBox(
                  height: 24,
                ),
                SizedBox(
                    height: 72,
                    width: MediaQuery.of(context).size.width - 48,
                    child: CustomTextField(
                      onChange: (te) {
                        setState(() {});
                      },
                      label: 'Confirm Password',
                      hint: '**********',
                      isObscure: obs,
                      controller: passc,
                      onTapSuffix: () {
                        setState(() {
                          if (obs == true) {
                            obs = false;
                          } else {
                            obs = true;
                          }
                        });
                      },
                    )),
                SizedBox(
                  height: 24,
                ),
                SizedBox(
                  height: 56,
                ),
                SizedBox(
                  height: 46,
                  width: MediaQuery.of(context).size.width - 48,
                  child: FilledButton(
                    onPressed: (isValid() != true)
                        ? null
                        : () async {
                            try {
                              var res = await verifyOtp(pass.text, email.text  );
                              if(res != null){
                              user = res.user;}
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => NewPass()));
                            } on AuthException catch (e) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(content: Text(e.message)));
                            }
                          },
                    child: Text('Set New Password'),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width - 48,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Remember password? Back to',
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 14,
                            color: Color(0xFFA7A7A7)),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => SignIn()));
                        },
                        child: Text(
                          'Sign in',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Color(0xFF0560FA)),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 28,
                )
              ],
              crossAxisAlignment: CrossAxisAlignment.start,
            ),
            SizedBox(
              width: 24,
            )
          ],
        ),
      ),
    );
  }
}
