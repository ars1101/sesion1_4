import 'package:flutter/material.dart';

class CustomCard extends StatelessWidget{
  final String label;
  final String hint;
  final String image;

  CustomCard({required this.label, required this.hint, required this.image}){}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container( decoration: BoxDecoration(boxShadow: [BoxShadow(color: Color(
        0x26000000, ), offset: Offset(0,2), blurRadius: 5)], color: Colors.white ),
        child: ListTile(

          leading: Image.asset(image),
          title: Text(label, style: TextStyle(color: Color(0xFF3A3A3A), fontWeight: FontWeight.w500, fontSize: 16),),
          subtitle: Text(hint, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Color(0xFFA7A7A7)),),
          trailing: Image.asset('assets/arrow.png'),



        ),
      ),
    );
  }


}