

import 'package:session1_4/onboarding/data/origindata.dart';

  class Queue{
  final List<Map<String,String>> _data = [];

  void push(Map<String, String> elem){
  var count = _data.length;
  _data.insert(count, elem);
  }

  Map<String, String> next(){
    var elem = _data[0];
    _data.remove(elem);
    return elem;
  }

  Map<String, String> peek(){
    var elem = _data[0];
    return elem;
  }

  bool isEmpty(){
    var count = _data.length;

    return count == 0;
  }

  int length(){
    return _data.length;
  }

  void resetQueue(){
  _data.clear();
  _data.addAll(originData);
  }}



