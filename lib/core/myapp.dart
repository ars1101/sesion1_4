import 'package:flutter/material.dart';
import 'package:session1_4/core/theme.dart';
import 'package:session1_4/onboarding/presentation/onboarding.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: light,
      home: const OnBoarding(),
    );
  }
}