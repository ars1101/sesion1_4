import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:session1_4/auth/data/repository/supawidgets.dart';
import 'package:session1_4/auth/prestation/pages/forgotpass.dart';
import 'package:session1_4/auth/prestation/pages/otp.dart';
import 'package:session1_4/auth/prestation/pages/signin.dart';
import 'package:session1_4/auth/prestation/pages/signup.dart';
import 'package:session1_4/auth/prestation/widgets/textfield.dart';
import 'package:session1_4/core/color.dart';
import 'package:session1_4/home/presentation/widgets/customcard.dart';
import 'package:session1_4/main.dart';
import 'package:session1_4/onboarding/presentation/holder.dart';
import 'package:session1_4/onboarding/repository/queue.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});


  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  String name = 'jjj';
  double balance = 0.00;
  bool value = false;


  @override
  void initState() {
    super.initState();
    setState(() {
      getUser().then((value) => {
        setState(() {

          name = value["fullname"].toString();

        })
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start,
          children: [
          Container( decoration: BoxDecoration(boxShadow: [BoxShadow(color: Color(
            0x26000000, ), offset: Offset(0,2), blurRadius: 5)], color: Colors.white ),
            width: MediaQuery.of(context).size.width, height: 108, child: Column(crossAxisAlignment: CrossAxisAlignment.center,
            children: [SizedBox(height: 69,), Text(
              "Profile",
              style: TextStyle(fontSize: 16, color: grey2),
            ),],
            ),
        
          ),
          Row(mainAxisAlignment: MainAxisAlignment.start,
            children: [SizedBox(width: 24,), Column( crossAxisAlignment: CrossAxisAlignment.start,
              children: [
            SizedBox(height: 27,),
            Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                SizedBox(
                  height: 60,
                  width: 60,
                  child: Image.asset(
                    "assets/ken.png",
                    scale: 0.4,
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Hello " + name.toString(),
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                    ),
                    Row(
                      children: [
                        Text(
                          'Current balance: ',
                          style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                        ),
                        Text(
                          'N' + balance.toString(),
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w500,
                              color: Color(0xFF0560FA)),
                        ),
                        SizedBox(
                          width: 111,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 19,
                    ),
                  ],
                ),
              ],
            ),
        
              SizedBox(
                height: 19,
              ),
              SizedBox( width: MediaQuery.of(context).size.width -48,
                child: Row(
                  children: [
                    SizedBox(
                      width: 24,
                    ),
                    Text(
                      "Enable Dark Theme",
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                    ),
                    Spacer(),
                    SizedBox( height: 26, width: 43,
                      child: Switch(
                          value: value,
                          onChanged: (val) {
                            setState(() {
                              value = val;
                            });
                          }),
                    ),
                    SizedBox(
                      width: 27,
                    ),
                  ],
                  mainAxisAlignment: MainAxisAlignment.start,
                ),
              ),
        
              SizedBox(
                height: 19,
              ),
              SizedBox(
                  height: 62,
                  width: MediaQuery.of(context).size.width - 48,
                  child: CustomCard(
                      image: 'assets/prof.png',
                      hint: 'Name, phone no, address, email ...',
                      label: 'Edit profile')),
              SizedBox(
                height: 12,
              ),
              SizedBox(
                  height: 62,
                  width: MediaQuery.of(context).size.width - 48,
                  child: CustomCard(
                    image: 'assets/stat.png',
                    label: 'Statements & Reports',
                    hint: 'Download transaction details, orders, deliveries',
                  )),
              SizedBox(
                height: 12,
              ),SizedBox(
                  height: 62,
                  width: MediaQuery.of(context).size.width - 48,
                  child: CustomCard(
                    image: 'assets/not.png',
                    label: 'Notification Settings',
                    hint: 'mute, unmute, set location & tracking setting',
                  )),
              SizedBox(
                height: 12,
              ),
              SizedBox(
                  height: 62,
                  width: MediaQuery.of(context).size.width - 48,
                  child: CustomCard(
                    image: 'assets/not.png',
                    label: 'Card & Bank account settings',
                    hint: 'change cards, delete card details',
                  )),
              SizedBox(
                height: 12,
              ),
              SizedBox(
                  height: 62,
                  width: MediaQuery.of(context).size.width - 48,
                  child: CustomCard(
                    image: 'assets/ref.png',
                    label: 'Refferals',
                    hint: 'check no of friends and earn',
                  )),
              SizedBox(
                height: 12,
              ),
              SizedBox(
                  height: 62,
                  width: MediaQuery.of(context).size.width - 48,
                  child: CustomCard(
                    image: 'assets/map.png',
                    label: 'About Us',
                    hint: 'know more about us, terms and conditions',
                  )),
              SizedBox(
                height: 12,
              ),
              GestureDetector( onTap: (){supabase.auth.signOut();
              },
                child: SizedBox(
                    height: 62,
                    width: MediaQuery.of(context).size.width - 48,
                    child: CustomCard(
                      image: 'assets/logout.png',
                      label: 'Log Out',
                      hint: '',
                    )),
              ),
              SizedBox(
                height: 12,
              ),
        
        
          ],), SizedBox(width: 24,)],),
        ],),
      ) ,);
  }
}