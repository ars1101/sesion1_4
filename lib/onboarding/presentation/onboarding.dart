import 'package:flutter/material.dart';
import 'package:session1_4/auth/prestation/pages/signup.dart';
import 'package:session1_4/onboarding/presentation/holder.dart';
import 'package:session1_4/onboarding/repository/queue.dart';

class OnBoarding extends StatefulWidget {
  const OnBoarding({super.key});


  @override
  State<OnBoarding> createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {

  Queue queue = Queue();
  int fullen = 0;
  Map<String, String> elem = {'': ''};
  PageController pcr = PageController();

  @override
  void initState() {
  setState(() {
    queue.resetQueue();
      super.initState();
      fullen = queue.length();
      elem = queue.peek();
  });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView.builder(physics: NeverScrollableScrollPhysics(),
        controller: pcr,
        onPageChanged: (aaa) async {setState(() {
  elem =  queue.next();
  elem = queue.peek();
});

},
        itemBuilder: (_, ind){
        return SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  height: 111,
                ),
                Image.asset(elem['image']!),
                SizedBox(
                  height: 48,
                ),
                Text(
                  elem['title']!,
                  style: TextStyle(
                      color: Color(0xFF0560FA),
                      fontWeight: FontWeight.w700,
                      fontSize: 24),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  elem['subtitle']!,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF3A3A3A)),
                  textAlign: TextAlign.center,
                ),
                Spacer(),
                (queue.length() != 1)?
                     Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 100,
                          height: 50,
                          child: OutlinedButton(
                            onPressed: () {Navigator.of(context).push(MaterialPageRoute(builder: (context) => SignUp()));},
                            child: Text(
                              'Skip',
                              style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.w700,
                                color: Color(0xFF0560FA),
                              ),
                            ),
                            style: OutlinedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                backgroundColor: Colors.white,
                                foregroundColor: Color(0xFF0560FA),
                                side: BorderSide(color: Color(0xFF0560FA))),
                          ),
                        ),
                        SizedBox(
                          width: 142,
                        ),
                        SizedBox(height: 50, width: 100,
                          child: FilledButton(
                              onPressed: () {
                                setState(() {
                                  pcr.nextPage(duration: Duration(milliseconds: 500), curve: Curves.decelerate);
                                });
                              },
                              child: Text('Next')),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 100,
                    )
                  ],
                )
                    : Column(
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width - 48,
                      height: 46,
                      child: FilledButton(
                        child: Text('Sign Up'),
                        onPressed: () {queue.next();
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => SignUp()));},
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row( mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Already have an account?',
                          style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: Color(0xFFA7A7A7)),
                        ),
                        GestureDetector( onTap: (){queue.next();
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => SignUp()));},
                          child: Text(
                            'Sign In',
                            style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 14,
                                color: Color(0xFF0560FA)),
                          ),
                        )
                      ],
                    ), SizedBox(height: 64,)
                  ],
                ),
              ]),
        );
      }, itemCount: fullen,),
    );
  }
}