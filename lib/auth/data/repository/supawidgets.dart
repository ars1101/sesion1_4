import 'package:flutter/material.dart';
import 'package:session1_4/main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';


Future<User?> regIn(name,password,email,phone) async {
  var res = await supabase.auth.signUp(password: password, email: email);
  User? user = res.user;
  supabase.from('profiles').insert({
    'fullname': name,
    'phone' : phone,
    'avatar' : '',
  });
  return user;
}

Future<AuthResponse?> logIn(password,email) async {
  var res = await supabase.auth.signInWithPassword(
      password: password, email: email);
  return res;
}

void sendOtp(email) async {
  supabase.auth.resetPasswordForEmail(email);
}

Future<AuthResponse?> verifyOtp(email, otp)async {
  var res = supabase.auth.verifyOTP(token: otp, type: OtpType.email, email: email);
  return res;
}

void changePass(newpass)async {
  await supabase.auth.updateUser(UserAttributes(password: newpass));
}

Future<String> sendOrder(userid, addres, country, phone, others, package_items, weight_items, worth_items, count)async {
  var res = await supabase.from('orders').insert({
    'id_user': userid,
    'addres' : addres,
    'country' : country,
    'others' : others,
    'package_items' : package_items,
    'weight_items' : weight_items,
    'worth_items' : worth_items,
    'delivery_charges' : count*2500,
    'instant_delivery' : 300,
    'tax_and_service_charges' : (count*2500 + 300)*0.05
  }).select('id').single();
  return res['id'];
}

Future<Map<String, dynamic>> getUser() async {
  return await supabase
      .from("profiles")
      .select()
      .eq("id_user", supabase.auth.currentUser!.id)
      .single();
}