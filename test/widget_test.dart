// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:session1_4/core/myapp.dart';

import 'package:session1_4/main.dart';
import 'package:session1_4/onboarding/data/origindata.dart';
import 'package:session1_4/onboarding/presentation/holder.dart';
import 'package:session1_4/onboarding/presentation/onboarding.dart';
import 'package:session1_4/onboarding/repository/queue.dart';

void main() {
  Queue queue = Queue();

  testWidgets('−	Изображение и текста из очереди извлекается правильно (в порядке добавления в очередь)', (WidgetTester tester) async {
  queue.resetQueue();
  for (var i in originData){
    expect(queue.next(), i );
  }
  });

  testWidgets('−	Корректное извлечение элементов из очереди (количество элементов в очереди уменьшается на единицу).', (WidgetTester tester) async {
    queue.resetQueue();
    int c = queue.length();
    queue.next();
    expect(queue.length(), c-1);
  });

  testWidgets('−	В случае, когда в очереди несколько картинок, устанавливается правильная надпись на кнопке.', (WidgetTester tester) async {
    queue.resetQueue();
    final pageView = find.byType(PageView);
    await tester.runAsync(() => tester.pumpWidget(const MaterialApp(
      home: OnBoarding(),
    )));
    var elem = queue.next();
    await tester.pumpAndSettle();
    expect(find.widgetWithText(FilledButton, "Next"), findsOneWidget);

  });

  testWidgets('−	в случае когда в очереди больше нет картинок, кнопка Skip отсутсвует', (WidgetTester tester) async {
    queue.resetQueue();
    final pageView = find.byType(PageView);
    await tester.runAsync(() => tester.pumpWidget(const MaterialApp(
      home: OnBoarding(),
    )));

    while(queue.length()>1) {
      queue.next();
    }
    if(queue.length()==0){
      await tester.pumpAndSettle();
      expect(find.widgetWithText(OutlinedButton, "Skip"), findsNothing);}
  });

  testWidgets('−	в случае, когда в очереди есть несколько картинок, кнопка sign in отсутвует ', (WidgetTester tester) async {
    queue.resetQueue;
    int cur = queue.length();
    for(int i = 0; i<cur; i++ ){
      if(queue.length() > 1){
        expect(find.widgetWithText(GestureDetector, 'Sign In' ), findsNothing);
      }
    }

  });
}

