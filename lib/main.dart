import 'package:flutter/material.dart';
import 'package:session1_4/core/myapp.dart';
import 'package:session1_4/onboarding/presentation/onboarding.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

void main() async {
  await Supabase.initialize(
    url: 'https://lwtgrvmdvlhofehpmgtb.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Imx3dGdydm1kdmxob2ZlaHBtZ3RiIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDcyMzc4NDYsImV4cCI6MjAyMjgxMzg0Nn0.V6mo8ojP3m2HGKgLzNItYp4k-Mfylz5DYLmEIodd1v0',
  );
  runApp(const MyApp());
}

final supabase = Supabase.instance.client;


